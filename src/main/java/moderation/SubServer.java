package moderation;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.io.*;
import java.net.Socket;
import java.util.Queue;

class SubServer extends Thread {

    private Socket socket; // сокет, через который сервер общается с клиентом
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток завписи в сокет
    private HazelcastInstance instance;
    Queue<Comment> queue;


    SubServer(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        Config cfg = new Config();
        instance = Hazelcast.newHazelcastInstance(cfg);
        queue = instance.getQueue("comments");

        start();
    }

    @Override
    public void run() {
        String entry, nick;
        try {
            nick = in.readLine();
            out.write("Welcome " + nick + "\n");
            out.flush();
            try {
                while (true) {
                    entry = in.readLine();
                    System.out.println(nick + " " + entry);

                    queue.offer(new Comment(entry, nick));

                }
            } catch (NullPointerException ignored) {
            }
        } catch (IOException e) {
            this.downService();
        }
    }

    public void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }

    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (SubServer el : MainServer.subServers) {
                    if (el.equals(this)) el.interrupt();
                    MainServer.subServers.remove(this);
                }
            }
        } catch (IOException ignored) {
        }
    }
}
