package moderation;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainServer {
    private static final int PORT = 8080;
    static List<SubServer> subServers = new CopyOnWriteArrayList<>();

    public static void main(String[] args) throws IOException {
        try (ServerSocket server = new ServerSocket(PORT)) {
            System.out.println("Server created");

            new SendServer();

            while (true) {
                //блокируется до возникновения нового соединения
                Socket socket = server.accept();
                try {
                    subServers.add(new SubServer(socket));
                } catch (IOException e) {
                    socket.close();
                }
            }
        }
    }
}