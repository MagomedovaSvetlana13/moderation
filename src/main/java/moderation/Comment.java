package moderation;

import java.io.Serializable;

public class Comment implements Serializable {
    private String comment;
    private String nick;

    public Comment() {

    }

    public Comment(String comment, String nick) {
        this.comment = comment;
        this.nick = nick;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
}
