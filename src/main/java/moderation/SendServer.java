package moderation;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Queue;

public class SendServer extends Thread {

    private HazelcastInstance instance;
    Queue<Comment> checkedComments;


    SendServer() {

        Config cfg = new Config();
        instance = Hazelcast.newHazelcastInstance(cfg);

        start();
    }

    @Override
    public void run() {
        Comment comment;
        while (true) {
            checkedComments = instance.getQueue("checkedComments");

            while (checkedComments.peek() != null) {
                comment = checkedComments.poll();
                //разослать сообщение по всем клиентам
                for (SubServer vr : MainServer.subServers) {
                    vr.send(comment.getNick() + " : " + comment.getComment());
                }
            }
        }
    }
}
