package moderation;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Queue;

public class Moderator {

    public static void main(String[] args) {

        ClientConfig clientConfig = new ClientConfig();
        HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
        Queue<Comment> checkedComments = client.getQueue("checkedComments");

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        try {
            Comment comment;
            String moderAnswer;
            while (true) {
                IQueue<Comment> queue = client.getQueue("comments");
                while (queue.peek() != null) {
                    comment = queue.poll();

                    System.out.println("//////////New comment!");
                    System.out.println(comment.getNick() + " add new comment: " + comment.getComment());

                    moderAnswer = input.readLine();
                    if (moderAnswer.equals("y")) {
                        checkedComments.offer(comment);

                        System.out.println("//////////Wait for next comment!");
                    } else {
                        System.out.println("//////////Correct the comment!");

                        comment.setComment(input.readLine());
                        checkedComments.offer(comment);

                        System.out.println("//////////Wait for next comment!");
                    }
                }
                System.out.println("//////////No new comments");
                Thread.sleep(5 * 1000);
            }
        } catch (Exception e) {
            client.shutdown();
        }
    }
}
