package moderation;

import java.io.*;
import java.net.Socket;

class SubClient {

    private Socket socket;
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток чтения в сокет
    private BufferedReader input; // поток чтения с консоли
    private String nickname; // имя клиента

    SubClient(String host, int port) {
        try {
            this.socket = new Socket(host, port);
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
        try {
            // потоки чтения из сокета/записи в сокет и чтения с консоли
            input = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.enterNickname();
            new ReadMsg().start();
            new WriteMsg().start();
        } catch (IOException e) {
            SubClient.this.downService();
        }
    }

    private void enterNickname() {
        System.out.print("Your nickname: ");
        try {
            nickname = input.readLine();
            out.write(nickname+"\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }

    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {
        }
    }

    // нить, читающая сообщения с сервера
    private class ReadMsg extends Thread {
        @Override
        public void run() {
            String str;
            try {
                while (true) {
                    str = in.readLine(); // ждем сообщения с сервера
                    if (str.equals("exit")) {
                        SubClient.this.downService();
                        break;
                    }
                    System.out.println(str);
                }
            } catch (IOException e) {
                SubClient.this.downService();
            }
        }
    }

    // нить, отправляющая сообщения, приходящие с консоли на сервер
    public class WriteMsg extends Thread {
        @Override
        public void run() {
            while (true) {
                String userWord;
                try {
                    userWord = input.readLine(); // сообщения с консоли
                    if (userWord.equals("exit")) {
                        out.write("exit" + "\n");
                        SubClient.this.downService();
                        break; // выходим из цикла если пришло "stop"
                    } else {
                        String message = userWord + "\n";
                        out.write(message); // отправляем на сервер
                    }
                    out.flush();
                } catch (IOException e) {
                    SubClient.this.downService();

                }

            }
        }
    }

}